#!/bin/bash
# 
# /lefteris/scripts/check_paris.sh
#
# Check PARIS application availability and restart it if the http return code is other than 200 or 
# the soap members request is not integer.
#
# Requirements: 
#   * An xml file with the request options
#   * The paris app admin passwd in the same location as the script (.glassfishpasswd)
#   * Root privileges
#   * curl util installed 
#
# This script has been created and can only be used for Bestseller E-commerce servers or services.
#
# Lefteris Agrianitis, January 12th, 2015
#

Sendpoint=http://member.bestseller.com:8080/ParisSoapService/ParisSoap
Sheader1="Content-Type: text/xml;charset=UTF-8"
Smethod="SOAPAction:getISSUMemberCount"
Sdata=soap.xml
soaprequest=`timeout 5s curl --silent --header "$Sheader1" --header $Smethod --data @$Sdata $Sendpoint`
filterrequest=$(echo "$soaprequest" | sed -e 's/\(.*\)<return>//' -e 's/<\/return>.*//')
pserviceURL="http://member.bestseller.com:8080/ParisSoapService/ParisSoap?WSDL"
pservice=`curl --write-out "%{http_code}\n" --silent --output /dev/null $pserviceURL`

# Check if stdout return is integer 

function isint() {
    return $(test "$@" -eq "$@" > /dev/null 2>&1);
}

# function to restart paris (you need the password file in the same folder as the script)

function restart_paris(){
    kill -9 `ps aux | grep java | grep -v grep | awk '{print $2}'`
    sleep 5
    /etc/init.d/httpd restart
    sleep 5
    /var/glassfish/bin/asadmin start-domain PARIS_PROD
    sleep 10
    /var/glassfish/bin/asadmin --user admin --passwordfile .glassfishpasswd  start-local-instance --node bestseller25 --sync normal i25
}

# If the soap service return code is not '200' -OR- the get members soap request is not integer the restart paris
# and log results in /var/log/check_paris_failed(or lastchecked)

if [[ $pservice = '200' ]] || isint $filterrequest;
then
    echo -e "`date` -  Number of members registered ${filterrequest} - Paris Service return code: ${pservice}" >> /var/log/check_paris_lastchecked
    exit 1;
else
    echo -e "`date`" >> /var/log/check_paris_failed
    restart_paris >> /var/log/check_paris_failed
    chown -R glassfish_as:glassfish_as /var/glassfish
fi
